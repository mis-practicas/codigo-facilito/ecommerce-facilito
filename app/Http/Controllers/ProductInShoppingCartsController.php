<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\ProductInShoppingCart;

class ProductInShoppingCartsController extends Controller
{
    public function __construct()
    {
        $this->middleware('shopping_cart');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->shopping_cart;
        $in_shopping_cart = ProductInShoppingCart::create([
            'shopping_cart_id' => $request->shopping_cart->id,
            'product_id' => $request->product_id,

        ]);

        if ($in_shopping_cart) 
        {
            return redirect()->back();
        }

        return redirect()->back()->withErrors(['product' => 'No se pudo agregar el producto']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
