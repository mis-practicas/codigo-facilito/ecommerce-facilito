<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Utilizado para serializar productos que se van a retornar a un componente vue
use App\Http\Resources\ProductsCollection;

class ShoppingCartController extends Controller
{
    public function __construct()
    {
        $this->middleware('shopping_cart');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        return view('shopping_cart.show', ['shopping_cart' => $request->shopping_cart]);
    }

    public function products(Request $request)
    {
        return new ProductsCollection($request->shopping_cart->products()->get());
    }
}
