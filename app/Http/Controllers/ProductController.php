<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Product;
// Utilizado para serializar productos que se van a retornar a un componente vue
use App\Http\Resources\ProductsCollection;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //  Muestra una colección del recurso.
        $products = Product::paginate(15);

        // Este metodo devuelve verdadero cuando se interpreta que el cliente este tipo de datos (Json) del servidor
        if ($request->wantsJson())
        {
            // ->toJson es un metodo que viene con elequent para serializar el resultado de una consulta al formato Json
            return new ProductsCollection($products);
        }

        return view('products.index', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //  Muestra un formulario para crear nuevos recursos.
        $product = new Product();
        return view('products.create', ['product' => $product]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  Almacenar en la base de datos nuevos recursos.
        $options = [
            'title' => $request->title,
            'price' => $request->price,
            'description' => $request->description
        ];

        if (Product::create($options)) {
            return redirect('/productos');
        }
        else {
            return view('products.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Muestra un recurso
        $product = Product::find($id);
        return view('products.show', ['product' => $product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //  Muestra un formulario para editar el recurso
        $product = Product::find($id);
        return view('products.edit', ['product' => $product]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //  Actualiza un recurso en especifico
        $product = Product::find($id);

        $product->title = $request->title;
        $product->price = $request->price;
        $product->description = $request->description;

        if ($product->save())
        {
            return redirect('/');
        }
        else
        {
            return view('products.edit', ['product' => $product]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Product::destroy($id);
        return redirect('/productos');
    }
}
