@extends('layouts.app')

@section('content')
    <div class="row justify-content-sm-center">
        {{-- xs (movil) sm (ligeramente más grandes) md (medianas) lg (grandes o desktop) --}}
        <div class="col-xs-12 col-sm-10 col-md-7 col-lg-6">
            <div class="card">
                <header class="padding text-center bg-primary">
                </header>
                <div class="card-body padding">
                    <h1 class="card-title">{!! $product->title !!}</h1>
                    <p>{!! $product->price !!}</p>
                    <div class="card-actions">
                        <add-product-btn :product='{!! json_encode($product) !!}'></add-product-btn>
                        @include('products.delete')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection