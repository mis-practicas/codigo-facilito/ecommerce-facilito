@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card padding">
            <div class="card-header">
                Crear producto
            </div>

            <div class="card-body">
                @include('products.form')
            </div>
        </div>
    </div>
@endsection