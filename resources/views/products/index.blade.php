@extends('layouts.app')

@section('content')
    <div class="container">
        <h1></h1>
        <div>
            <products-component></products-component>
        </div>
        <div class="actions text-center">
            {!! $products->links() !!}
        </div>
    </div>
@endsection